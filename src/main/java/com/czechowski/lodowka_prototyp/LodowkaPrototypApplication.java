package com.czechowski.lodowka_prototyp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LodowkaPrototypApplication {

    public static void main(String[] args) {
        SpringApplication.run(LodowkaPrototypApplication.class, args);
    }

}
