package com.czechowski.lodowka_prototyp.lodowka.models;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
@Data
@Builder
public class Lodowka {

    int id;
    List<Skladnik> skladniki = new ArrayList<>();
}
