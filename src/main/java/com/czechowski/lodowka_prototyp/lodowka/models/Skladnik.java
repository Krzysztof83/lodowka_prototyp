package com.czechowski.lodowka_prototyp.lodowka.models;

import lombok.Builder;
import lombok.Data;

/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
@Data
@Builder
public class Skladnik {

    String name;
    Double amount;
    String image;
    int adddss;
}
