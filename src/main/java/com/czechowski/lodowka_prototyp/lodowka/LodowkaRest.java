package com.czechowski.lodowka_prototyp.lodowka;

import com.czechowski.lodowka_prototyp.lodowka.models.Lodowka;
import com.czechowski.lodowka_prototyp.lodowka.services.LodowkaService;
import com.czechowski.lodowka_prototyp.lodowka.services.LodowkaServiceMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
@RestController
@RequestMapping("/lodowka")
public class LodowkaRest {

    @Autowired
    LodowkaService lodowkaService;

    public Lodowka getLodowka(int id) {
        System.out.println("aaaaasaaaaa");
        return lodowkaService.getLodowka(id);
    }

}
