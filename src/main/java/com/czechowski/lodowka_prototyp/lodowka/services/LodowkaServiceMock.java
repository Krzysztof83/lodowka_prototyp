package com.czechowski.lodowka_prototyp.lodowka.services;

import com.czechowski.lodowka_prototyp.lodowka.models.Lodowka;
import com.czechowski.lodowka_prototyp.lodowka.models.Skladnik;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;


/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
@Service
public class LodowkaServiceMock implements LodowkaService {
    @Override
    public Lodowka getLodowka(int id) {
        return null;
    }

    private Lodowka getLodowkaById(int id) {
        switch (id) {
            case 1:
                return getLodowka1();
            case 2:
                return getLodowka2();
            default:
                return Lodowka.builder().build();
        }
    }

    private Lodowka getLodowka2() {
        return Lodowka.builder()
                .id(2)
                .skladniki(ImmutableList.of(
                        Skladnik.builder()
                                .name("Jajka")
                                .amount(Double.valueOf("4"))
                                .build(),
                        Skladnik.builder()
                                .name("Ogorek")
                                .amount(Double.valueOf("3"))
                                .build()))
                .build();
    }

    private Lodowka getLodowka1() {
        return Lodowka.builder()
                .id(1)
                .skladniki(ImmutableList.of(
                        Skladnik.builder()
                            .name("Makaron")
                            .amount(Double.valueOf("0.5"))
                            .build(),
                        Skladnik.builder()
                                .name("Mielone")
                                .amount(Double.valueOf("0.5"))
                                .build()))
                .build();
    }


}
