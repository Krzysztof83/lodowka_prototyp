package com.czechowski.lodowka_prototyp.lodowka.services;

import com.czechowski.lodowka_prototyp.lodowka.models.Lodowka;

/**
 * @author <a href="mailto:k.czechowski83@gmail.com">Krzysztof Czechowski</a>
 */
public interface LodowkaService {

    Lodowka getLodowka(int id);
}
